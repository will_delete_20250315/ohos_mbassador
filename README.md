## MBassador

## Overview

MBassador is a third-party library that utilizes a publish-subscribe pattern to provide the following features:

- Event publishing and subscription
- Synchronous and asynchronous event delivery
- Event filtering
-  Setting of weak and strong references for events
- Event annotation-driven reporting

## Download and Installation

```
ohpm install @ohos/mbassador
```
For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## Annotation

| Name |  Description |
|------|------|
| @Handler | Event handler. |
| @Listener | Custom listener scope, such as the reference type used. |

## Attributes

### @Handler

| Attribute |  Description |
|------|------|
| filters?: IMessageFilter<Object>[] | Event filter. |
| delivery?: Invoke | Synchronous and asynchronous event delivery. |
| priority?: number | Event priority. |
| enabled?: boolean | Whether an event is received. |

### @Listener

| Properties |  Description |
|------|------|
| className| Event listener name. |
| References | Strong and weak reference object. |

## Usage

### Listener

```
import { Annotations, References } from '@ohos/mbassador'
......

@Annotations.Listener("ExampleListener",References.Weak)
export class ExampleListener {
  @Annotations.handle()
  firstMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({priority:100000,enabled:false})
  secondMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({filters:[new ExampleFilter],enabled:true})
  thirdMessage(str: string, listener: ExampleListener) {
    //do something
  }
}
```

### Synchronous Method

```
    let mbassador = new MBassador<String>()
    let listener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is first")).now();
```

### Asynchronous Method

```
    let mbassador:MBassador = new MBassador()
    let listener:ExampleListener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is asynchronously message")).asynchronously();
```

### Unbind

```
   mbassador.unSubscribe(listener);
```

### Filter

```
import { IMessageFilter, SubscriptionContext } from '@ohos/mbassador'

export class ExampleFilter implements IMessageFilter<String> {
    accepts(msg: String, context: SubscriptionContext) {
          //some code
           return true;
    }
}
```
## Directory Structure

```
|---- Mbassador
      |----mbassador 
           |----src
                |---- bus
                |---- config
                |---- dispatch
                |---- entry
                |---- interface
                |---- listener
                |---- subscription
                |---- utils
      |----entry
           |----src
                |---- main
                      |---- ets
                            |---- entryability
                                  |---- EntryAbility.ts
                            |---- pages
                                  |---- index.ets
```

## Constraints

ohos_mbassador has been verified in the following versions:

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)
- DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## How to Contribute

If you find any problem when using ohos_mbassador, submit an [Issue](https://gitee.com/openharmony-sig/ohos-mbassador/issues) or a [PR](https://gitee.com/openharmony-sig/ohos-mbassador/pulls).


## License

This project is licensed under the terms of the [MIT License](https://gitee.com/openharmony-sig/ohos_mbassador/blob/master/LICENSE).
