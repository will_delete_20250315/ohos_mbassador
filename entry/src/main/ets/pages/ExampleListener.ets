/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { Annotations, References } from '@ohos/mbassador'
import { ExampleFilter } from './ExampleFilter'
import { CallBackListener } from './CallBackListener'

@Annotations.Listener("ExampleListener",References.Weak)
export class ExampleListener {
  call: CallBackListener;

  constructor(call: CallBackListener) {
    this.call = call;
  }

  @Annotations.handle()
  firstMessage(str: string, listener: ExampleListener) {
    console.log("asa_ ExampleListener firstMessage ----:" + str);
    listener.call.callData("ExampleListener", "firstMessage", str, 0);
  }

  @Annotations.handle({ priority: 100000, enabled: false })
  secondMessage(str: string, listener: ExampleListener) {
    console.log("asa_ ExampleListener secondMessage ----:" + str);
    listener.call.callData("ExampleListener", "secondMessage", str, 1);
  }

  @Annotations.handle({ filters: [new ExampleFilter()], enabled: true })
  thirdMessage(str: string, listener: ExampleListener) {
    console.log("asa_ ExampleListener thirdMessage ----:" + str);
    listener.call.callData("ExampleListener", "thirdMessage", str, 2);
  }
}
