/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Handler } from '../listener/Handler'
import type { IMessageFilter } from '../listener/IMessageFilter'
import { Invoke } from '../listener/Invoke'

export class MethodParameters {
    private methodName: String;
    private parameter: Handler;

    constructor(name: String, parameter: Handler) {
        this.methodName = name;
        this.parameter = parameter;
    }

    getMethodName() {
        return this.methodName;
    }

    getHandleParameter() {
        return this.parameter;
    }

    getFilters(): Array<IMessageFilter<Object>> {
        return this.getHandleParameter().filters;
    }

    isFiltered(): boolean {
        return this.getFilters() && this.getFilters().length > 0;
    }

    isAsynchronous(): boolean {
        return this.parameter.delivery.valueOf() == Invoke.Asynchronously.valueOf();
    }
}