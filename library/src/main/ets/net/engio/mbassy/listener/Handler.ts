/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { IMessageFilter } from './IMessageFilter';
import { Invoke } from './Invoke'

export type HandlerOptions = {
    filters?: IMessageFilter<Object>[],
    delivery?: Invoke,
    priority?: number,
    enabled?: boolean
}

export class Handler {
    filters: IMessageFilter<Object>[] = [];
    delivery: Invoke = Invoke.Synchronously;
    priority: number = 0;
    enabled: boolean = true;

    constructor(options: HandlerOptions) {
        const {filters,delivery,priority,enabled} = options || {};
        this.filters = filters || [];
        this.delivery = delivery || Invoke.Synchronously;
        this.priority = priority || 0;
        this.enabled = enabled || true;
    }
}